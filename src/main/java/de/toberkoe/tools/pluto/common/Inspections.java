package de.toberkoe.tools.pluto.common;

public class Inspections {

    public static <E> ObjectInspection<E> inspect(E object) {
        return new ObjectInspection<>(object);
    }

    public static NumberInspection inspect(Number number) {
        return new NumberInspection(number);
    }
}
