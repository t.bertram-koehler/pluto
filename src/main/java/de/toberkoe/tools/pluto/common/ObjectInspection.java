package de.toberkoe.tools.pluto.common;

public class ObjectInspection<E> {

    private final E inspectedObject;

    ObjectInspection(E inspectedObject) {
        this.inspectedObject = inspectedObject;
    }

    protected E getInspectedObject() {
        return inspectedObject;
    }

    public void isNotNull() {
        isNotNull("Object must not be null");
    }

    public void isNotNull(String message) {
        if (inspectedObject == null) {
            throw new IllegalArgumentException(message);
        }
    }
}
