package de.toberkoe.tools.pluto.common;

import java.util.function.Predicate;

public class NumberInspection extends ObjectInspection<Number> {

    NumberInspection(Number object) {
        super(object);
    }

    public void isPositive() {
        isPositive(String.format("Given number %d must be positive", getInspectedObject()));
    }

    public void isPositive(String message) {
        matchesCondition(n -> n.doubleValue() > 0, message);
    }

    public void isZero() {
        isZero(String.format("Given number %d must be zero", getInspectedObject()));
    }

    public void isZero(String message) {
        matchesCondition(n -> n.doubleValue() == 0, message);
    }

    public void isNegative() {
        isNegative(String.format("Given number %d must be negative", getInspectedObject()));
    }

    public void isNegative(String message) {
        matchesCondition(n -> n.doubleValue() < 0, message);
    }

    private void matchesCondition(Predicate<Number> condition, String errorMessage) {
        isNotNull();
        if (condition.test(getInspectedObject())) {
            return;
        }
        throw new IllegalArgumentException(errorMessage);
    }
}
