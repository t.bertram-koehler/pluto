package de.toberkoe.tools.pluto.common;

import java.util.Optional;

public class Throwables {
    public static <T extends Throwable> Optional<T> extractCause(Throwable exception, Class<T> causeClass) {
        if (exception == null) {
            return Optional.empty();
        } else if (causeClass.isInstance(exception)) {
            return Optional.of((T) exception);
        } else {
            return extractCause(exception.getCause(), causeClass);
        }
    }
}
