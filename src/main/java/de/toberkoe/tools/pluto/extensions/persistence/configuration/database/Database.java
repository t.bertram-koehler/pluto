package de.toberkoe.tools.pluto.extensions.persistence.configuration.database;

public enum Database {
    HSQLDB;

    //FIXME create more flexible datasource
    //FIXME support oracle?

    public DataSourceProvider dataSourceProvider() {
        switch (this) {
            case HSQLDB:
                return new HSQLDBDataSourceProvider();
            default:
                throw new UnsupportedOperationException("[" + this + "] DataSourceProvider not yet implemented");
        }
    }
}
