package de.toberkoe.tools.pluto.extensions.persistence.configuration.database;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface UseDatabases {

    UseDatabase[] values();
}
