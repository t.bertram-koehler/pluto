package de.toberkoe.tools.pluto.extensions.persistence.configuration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Log {

    enum Level {
        OFF, FATAL, ERROR, WARN, INFO, DEBUG, TRACE
    }

    Level value() default Level.OFF;


}
