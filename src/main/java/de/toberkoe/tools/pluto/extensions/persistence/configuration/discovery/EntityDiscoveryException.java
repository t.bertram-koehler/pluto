package de.toberkoe.tools.pluto.extensions.persistence.configuration.discovery;

public class EntityDiscoveryException extends RuntimeException {

    public EntityDiscoveryException(String message) {
        super(message);
    }
}
