package de.toberkoe.tools.pluto.extensions.persistence;

import de.toberkoe.tools.pluto.extensions.mocking.MockTest;
import de.toberkoe.tools.pluto.extensions.persistence.configuration.IntegrationTestConfig;
import de.toberkoe.tools.pluto.extensions.persistence.setup.PersistenceManager;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class PersistenceTest extends MockTest implements BeforeAllCallback, AfterAllCallback {

    private final PersistenceManager manager;

    public PersistenceTest() {
        this.manager = new PersistenceManager();
    }

    @Override
    public void beforeAll(ExtensionContext context) throws Exception {
        IntegrationTestConfig config = IntegrationTestConfig.build(context.getRequiredTestClass());
        manager.init(config);
    }

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        super.beforeEach(context);
        //FIXME speed up by explicit naming of injectable services
        PersistenceManager.injectAll(context.getTestInstance());
    }

    @Override
    public void afterAll(ExtensionContext context) {
        manager.close();
    }
}
