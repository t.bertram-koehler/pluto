package de.toberkoe.tools.pluto.extensions.persistence.configuration.discovery;

public enum Strategy {
    DYNAMIC, STATIC
}
