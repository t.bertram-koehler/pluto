package de.toberkoe.tools.pluto.extensions.persistence.configuration.database;

import java.util.Properties;
import javax.sql.DataSource;

public interface DataSourceProvider {

    String hibernateDialect();

    DataSource dataSource();

    Class<? extends DataSource> dataSourceClassName();

    Properties dataSourceProperties();

    String url();

    String username();

    String password();

    Database database();

    enum IdentifierStrategy {
        IDENTITY,
        SEQUENCE
    }
}