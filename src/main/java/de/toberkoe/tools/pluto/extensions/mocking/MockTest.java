package de.toberkoe.tools.pluto.extensions.mocking;

import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.mockito.MockitoAnnotations;

public class MockTest implements BeforeEachCallback {

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        context.getTestInstance().ifPresent(MockitoAnnotations::initMocks);
    }
}
