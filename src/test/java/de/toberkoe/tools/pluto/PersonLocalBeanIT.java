package de.toberkoe.tools.pluto;

import de.toberkoe.tools.pluto.examples.persons.Person;
import de.toberkoe.tools.pluto.examples.persons.PersonLocalBean;
import de.toberkoe.tools.pluto.extensions.persistence.InjectPersistence;
import de.toberkoe.tools.pluto.extensions.persistence.PersistenceTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.fest.assertions.api.Assertions.assertThat;

@ExtendWith(PersistenceTest.class)
class PersonLocalBeanIT {

    @InjectPersistence
    private PersonLocalBean repository;

    @Test
    void testCreatePerson() {
        Person merged = repository.create(new Person("Luke", "Skywalker"));
        assertThat(merged).isNotNull();
        assertThat(merged.getId()).isPositive();
    }
}
