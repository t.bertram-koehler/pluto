package de.toberkoe.tools.pluto;

import de.toberkoe.tools.pluto.examples.persons.Hobby;
import de.toberkoe.tools.pluto.examples.persons.Job;
import de.toberkoe.tools.pluto.examples.persons.Person;
import de.toberkoe.tools.pluto.examples.persons.PersonCdiService;
import de.toberkoe.tools.pluto.extensions.persistence.PersistenceTest;
import de.toberkoe.tools.pluto.extensions.persistence.configuration.discovery.EntityClassProvider;
import de.toberkoe.tools.pluto.extensions.persistence.configuration.discovery.EntityDiscoveryMode;
import de.toberkoe.tools.pluto.extensions.persistence.configuration.discovery.Strategy;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.fest.assertions.api.Assertions.assertThat;

@ExtendWith(PersistenceTest.class)
@EntityDiscoveryMode(value = Strategy.STATIC)
class StaticPersonCdiServiceIT {

    @Inject
    PersonCdiService service;

    @EntityClassProvider
    static List<Class<?>> provideEntityClasses() {
        return List.of(Person.class);
    }

    @EntityClassProvider
    static Set<Class<?>> provideEntityClassesSet() {
        return Set.of(Hobby.class);
    }

    @EntityClassProvider
    static Class<?>[] provideEntityClassesArray() {
        return new Class<?>[] { Job.class };
    }

    @Test
    void testCdiService() {
        Person merged = service.create(new Person("Luke", "Skywalker"));
        assertThat(merged).isNotNull();
        assertThat(merged.getId()).isPositive();
    }

}