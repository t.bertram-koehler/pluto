package de.toberkoe.tools.pluto;

import de.toberkoe.tools.pluto.examples.persons.Person;
import de.toberkoe.tools.pluto.examples.persons.PersonLocalBean;
import de.toberkoe.tools.pluto.extensions.mocking.MockTest;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockTest.class)
class PersonLocalBeanTest {

    @Mock
    private EntityManager manager;

    @InjectMocks
    private PersonLocalBean repository;

    @Test
    void shouldFailOnNullPerson() {
        assertThrows(IllegalArgumentException.class, () -> repository.create(null));
    }

    @Test
    void shouldFailOnPositiveId() {
        Person person = new Person();
        person.setId(1L);
        assertThrows(IllegalArgumentException.class, () -> repository.create(person));
    }

    @Test
    void shouldFailOnNegativeId() {
        Person person = new Person();
        person.setId(-1L);
        assertThrows(IllegalArgumentException.class, () -> repository.create(person));
    }

    @Test
    void testCreatePerson() {
        Person person = new Person();
        doReturn(person).when(manager).merge(any(Person.class));
        repository.create(person);
        verify(manager, atLeastOnce()).merge(person);
    }
}
