package de.toberkoe.tools.pluto;

import de.toberkoe.tools.pluto.examples.persons.Person;
import de.toberkoe.tools.pluto.examples.persons.PersonCdiService;
import de.toberkoe.tools.pluto.extensions.persistence.PersistenceTest;
import de.toberkoe.tools.pluto.extensions.persistence.configuration.Log;
import javax.inject.Inject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.fest.assertions.api.Assertions.assertThat;

@ExtendWith(PersistenceTest.class)
@Log(Log.Level.DEBUG)
class DynamicPersonCdiServiceIT {

    @Inject
    PersonCdiService service;

    @Test
    void testCdiService() {
        Person merged = service.create(new Person("Luke", "Skywalker"));
        assertThat(merged).isNotNull();
        assertThat(merged.getId()).isPositive();
    }

}