package de.toberkoe.tools.pluto.examples.persons;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import static de.toberkoe.tools.pluto.common.Inspections.inspect;

public class EntityCdiService<E> {

    @Inject
    private EntityManager manager;

    public E create(E person) {
        inspect(person).isNotNull();
        return manager.merge(person);
    }
}
