package de.toberkoe.tools.pluto.examples.persons;

import de.toberkoe.tools.pluto.common.Inspections;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class PersonLocalBean {

    @PersistenceContext
    private EntityManager manager;

    public Person create(Person person) {
        Inspections.inspect(person).isNotNull();
        Inspections.inspect(person.getId()).isZero();

        return manager.merge(person);
    }
}
